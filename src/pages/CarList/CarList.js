import '../../App.css';
import { useState, useEffect } from 'react';
import { v4 as uuidv4 } from 'uuid';
import Select from 'react-select';
import VehicleGroup from './VehicleGroup';

function CarList() {
  const options = [
    { value: 'sortAscending', label: 'Price (low to high)' },
    { value: 'sortDescending', label: 'Price (high to low)' },
  ];
  const [vehicleData, setVehicleData] = useState([]);
  const [sortByPrice, setSortByPrice] = useState(options[0]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    fetch('/ctabe/cars.json')
      .then((response) => response.json())
      .then((data) => {
        setVehicleData(data);
        setIsLoading(false);
      });
  }, []);

  if (isLoading) {
    return (<div>Loading...</div>);
  }

  return (
    <div className="app">
      <div className="sort-container">
        <Select
          value={sortByPrice}
          onChange={(e) => setSortByPrice(e)}
          options={options}
        />
      </div>
      <div className="car-group">
        {vehicleData.map((vehicleInfo) => (
          <VehicleGroup
            key={uuidv4()}
            sortByPrice={sortByPrice.value}
            vehicleRentalCore={vehicleInfo.VehAvailRSCore.VehRentalCore}
            vehicleVendorAvailability={vehicleInfo.VehAvailRSCore.VehVendorAvails}
          />
        ))}
      </div>
    </div>
  );
}

export default CarList;
