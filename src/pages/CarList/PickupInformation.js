import PropTypes from 'prop-types';
import moment from 'moment';

function PickupInformation({ vehicleRentalCore }) {
  const {
    '@PickUpDateTime': pickupDate,
    '@ReturnDateTime': returnDate,
    PickUpLocation: pickupLocation,
    ReturnLocation: returnLocation,
  } = vehicleRentalCore;
  const { '@Name': pickupLocationName } = pickupLocation;
  const { '@Name': returnLocationName } = returnLocation;

  function formatDate(date) {
    return moment(date).format('DD-MM-YYYY');
  }

  return (
    <legend>
      Pickup:
      {' '}
      {formatDate(pickupDate)}
      {' '}
      {pickupLocationName}
      <br />
      Return:
      {' '}
      {formatDate(returnDate)}
      {' '}
      {returnLocationName}
    </legend>
  );
}

PickupInformation.propTypes = {
  vehicleRentalCore: PropTypes.shape({
    '@PickUpDateTime': PropTypes.string.isRequired,
    '@ReturnDateTime': PropTypes.string.isRequired,
    PickUpLocation: PropTypes.shape({
      '@Name': PropTypes.string.isRequired,
    }),
    ReturnLocation: PropTypes.shape({
      '@Name': PropTypes.string.isRequired,
    }),
  }).isRequired,

};

export default PickupInformation;
