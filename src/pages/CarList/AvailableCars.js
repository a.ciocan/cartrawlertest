import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { v4 as uuidv4 } from 'uuid';
import CarResult from './CarResult';

function AvailableCars({ vehicleVendorAvailability, sortByPrice }) {
  function getVehicleWithVendorAndId(vehicle, vendor) {
    return { ...vehicle, vendor, id: uuidv4() };
  }

  function getCarList(vehiclesByVendors) {
    const carList = [];
    vehiclesByVendors.forEach((vendor) => {
      vendor.VehAvails.forEach((vehicle) => {
        const vehicleWithVendor = getVehicleWithVendorAndId(vehicle, vendor.Vendor);
        carList.push(vehicleWithVendor);
      });
    });
    return carList;
  }

  const carList = getCarList(vehicleVendorAvailability);
  const [sortedVehicles, setSortedVehicles] = useState([]);

  useEffect(() => {
    const sortArray = (sortByPrice) => {
      let sorted = [];

      if (sortByPrice === 'sortAscending') {
        sorted = [...carList].sort((a, b) => a.TotalCharge['@RateTotalAmount'] - b.TotalCharge['@RateTotalAmount']);
      } else {
        sorted = [...carList].sort((a, b) => b.TotalCharge['@RateTotalAmount'] - a.TotalCharge['@RateTotalAmount']);
      }
      setSortedVehicles(sorted);
    };
    sortArray(sortByPrice);
  }, [sortByPrice]);

  return (
    <>
      {sortedVehicles.map((car) => <CarResult key={car.id} id={car.id} car={car} />)}
    </>
  );
}

AvailableCars.propTypes = {
  sortByPrice: PropTypes.string.isRequired,
  vehicleVendorAvailability: PropTypes.arrayOf(PropTypes.shape({
    Vendor: PropTypes.shape({
      '@Code': PropTypes.string.isRequired,
      '@Name': PropTypes.string.isRequired,
    }),
    VehAvails: PropTypes.arrayOf(
      PropTypes.shape({
        '@Status': PropTypes.string.isRequired,
        Vehicle: PropTypes.shape({
          '@AirConditionInd': PropTypes.string.isRequired,
          '@TransmissionType': PropTypes.string.isRequired,
          '@FuelType': PropTypes.string.isRequired,
          '@DriveType': PropTypes.string.isRequired,
          '@PassengerQuantity': PropTypes.string.isRequired,
          '@BaggageQuantity': PropTypes.string.isRequired,
          '@Code': PropTypes.string.isRequired,
          '@CodeContext': PropTypes.string.isRequired,
          '@DoorCount': PropTypes.string.isRequired,
          VehMakeModel: PropTypes.shape({
            '@Name': PropTypes.string.isRequired,
          }),
          PictureURL: PropTypes.string.isRequired,
        }),
        TotalCharge: PropTypes.shape({
          '@RateTotalAmount': PropTypes.string.isRequired,
          '@EstimatedTotalAmount': PropTypes.string.isRequired,
          '@CurrencyCode': PropTypes.string.isRequired,
        }),
      }),
    ),
  })).isRequired,
};

export default AvailableCars;
