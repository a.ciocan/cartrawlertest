import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import CarPic from '../../components/CarPic';
import CarSpecs from '../../components/CarSpecs';
import CarVendor from '../../components/CarVendor';
import CarPrice from '../../components/CarPrice';

function CarResult({ car, id }) {
  const {
    vendor,
    '@Status': status,
    Vehicle: vehicle,
    TotalCharge: totalCharge,
  } = car;

  const {
    '@Code': vendorCode,
    '@Name': vendorName,
  } = vendor;

  const {
    '@AirConditionInd': airConditioning,
    '@TransmissionType': transmissionType,
    '@FuelType': fuelType,
    '@DriveType': driveType,
    '@PassengerQuantity': passengerQuantity,
    '@BaggageQuantity': baggageQuantity,
    '@Code': vehicleCode,
    '@CodeContext': vehicleCodeContext,
    '@DoorCount': doorCount,
    VehMakeModel: vehicleMakeModel,
    PictureURL: pictureURL,
  } = vehicle;

  const { '@Name': vehicleMakeName } = vehicleMakeModel;

  const {
    '@RateTotalAmount': rateTotalAmount,
    '@EstimatedTotalAmount': estimatedTotalAmount,
    '@CurrencyCode': currencyCode,
  } = totalCharge;

  const isAvailable = status === 'Available';

  const carInfo = {
    pictureURL,
    vehicleMakeName,
    vehicleCode,
    vehicleCodeContext,
    status,
    isAvailable,
    airConditioning,
    transmissionType,
    passengerQuantity,
    baggageQuantity,
    doorCount,
    fuelType,
    driveType,
    vendorName,
    vendorCode,
    rateTotalAmount,
    currencyCode,
    estimatedTotalAmount,
  };

  return (
    <div className="car-box">
      <div className="car-info">
        <CarPic pictureURL={pictureURL} />
        <CarSpecs
          vehicleMakeName={vehicleMakeName}
          vehicleCodeContext={vehicleCodeContext}
          vehicleCode={vehicleCode}
          status={status}
          isAvailable={isAvailable}
          airConditioning={airConditioning}
          transmissionType={transmissionType}
          passengerQuantity={passengerQuantity}
          baggageQuantity={baggageQuantity}
          doorCount={doorCount}
          fuelType={fuelType}
          driveType={driveType}
        />
      </div>
      <div className="car-footer-info">
        <CarVendor
          vendorName={vendorName}
          vendorCode={vendorCode}
        />
        <CarPrice
          rateTotalAmount={rateTotalAmount}
          currencyCode={currencyCode}
          estimatedTotalAmount={estimatedTotalAmount}
        />
        <div className="button-container">
          <Link className="details-button" to={`/car/${id}`} state={{ carInfo }}>View</Link>
        </div>
      </div>
    </div>
  );
}

CarResult.propTypes = {
  car: PropTypes.shape({
    '@Status': PropTypes.string.isRequired,
    Vehicle: PropTypes.shape({
      '@AirConditionInd': PropTypes.string.isRequired,
      '@TransmissionType': PropTypes.string.isRequired,
      '@FuelType': PropTypes.string.isRequired,
      '@DriveType': PropTypes.string.isRequired,
      '@PassengerQuantity': PropTypes.string.isRequired,
      '@BaggageQuantity': PropTypes.string.isRequired,
      '@Code': PropTypes.string.isRequired,
      '@CodeContext': PropTypes.string.isRequired,
      '@DoorCount': PropTypes.string.isRequired,
      VehMakeModel: PropTypes.shape({
        '@Name': PropTypes.string.isRequired,
      }),
      PictureURL: PropTypes.string.isRequired,
    }),
    TotalCharge: PropTypes.shape({
      '@RateTotalAmount': PropTypes.string.isRequired,
      '@EstimatedTotalAmount': PropTypes.string.isRequired,
      '@CurrencyCode': PropTypes.string.isRequired,
    }),
    vendor: PropTypes.shape({
      '@Code': PropTypes.string.isRequired,
      '@Name': PropTypes.string.isRequired,
    }),
  }).isRequired,
  id: PropTypes.string.isRequired,
};

export default CarResult;
