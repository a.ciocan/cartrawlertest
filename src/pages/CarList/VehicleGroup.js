import PropTypes from 'prop-types';
import PickupInformation from './PickupInformation';
import AvailableCars from './AvailableCars';

function VehicleGroup({
  vehicleRentalCore, vehicleVendorAvailability, sortByPrice,
}) {
  return (
    <fieldset className="pickup-information">
      <PickupInformation vehicleRentalCore={vehicleRentalCore} />
      <AvailableCars
        vehicleVendorAvailability={vehicleVendorAvailability}
        sortByPrice={sortByPrice}
      />
    </fieldset>
  );
}

VehicleGroup.propTypes = {
  sortByPrice: PropTypes.string.isRequired,
  vehicleRentalCore: PropTypes.shape({
    '@PickUpDateTime': PropTypes.string.isRequired,
    '@ReturnDateTime': PropTypes.string.isRequired,
    PickUpLocation: PropTypes.shape({
      '@Name': PropTypes.string.isRequired,
    }),
    ReturnLocation: PropTypes.shape({
      '@Name': PropTypes.string.isRequired,
    }),
  }).isRequired,
  vehicleVendorAvailability: PropTypes.arrayOf(PropTypes.shape({
    Vendor: PropTypes.shape({
      '@Code': PropTypes.string.isRequired,
      '@Name': PropTypes.string.isRequired,
    }),
    VehAvails: PropTypes.arrayOf(
      PropTypes.shape({
        '@Status': PropTypes.string.isRequired,
        Vehicle: PropTypes.shape({
          '@AirConditionInd': PropTypes.string.isRequired,
          '@TransmissionType': PropTypes.string.isRequired,
          '@FuelType': PropTypes.string.isRequired,
          '@DriveType': PropTypes.string.isRequired,
          '@PassengerQuantity': PropTypes.string.isRequired,
          '@BaggageQuantity': PropTypes.string.isRequired,
          '@Code': PropTypes.string.isRequired,
          '@CodeContext': PropTypes.string.isRequired,
          '@DoorCount': PropTypes.string.isRequired,
          VehMakeModel: PropTypes.shape({
            '@Name': PropTypes.string.isRequired,
          }),
          PictureURL: PropTypes.string.isRequired,
        }),
        TotalCharge: PropTypes.shape({
          '@RateTotalAmount': PropTypes.string.isRequired,
          '@EstimatedTotalAmount': PropTypes.string.isRequired,
          '@CurrencyCode': PropTypes.string.isRequired,
        }),
      }),
    ),
  })).isRequired,
};

export default VehicleGroup;
