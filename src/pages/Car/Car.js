import { useLocation } from 'react-router-dom';
import CarPic from '../../components/CarPic';
import CarSpecs from '../../components/CarSpecs';
import CarVendor from '../../components/CarVendor';
import CarPrice from '../../components/CarPrice';

function Car() {
  const location = useLocation();
  const { carInfo } = location.state;
  const {
    pictureURL,
    vehicleMakeName,
    vehicleCodeContext,
    vehicleCode,
    status,
    isAvailable,
    airConditioning,
    transmissionType,
    passengerQuantity,
    baggageQuantity,
    doorCount,
    fuelType,
    driveType,
    vendorName,
    vendorCode,
    rateTotalAmount,
    currencyCode,
    estimatedTotalAmount,
  } = carInfo;

  return (
    <div className="car-group">
      <div className="car-box">
        <div className="car-info">
          <CarPic pictureURL={pictureURL} />
          <CarSpecs
            vehicleMakeName={vehicleMakeName}
            vehicleCodeContext={vehicleCodeContext}
            vehicleCode={vehicleCode}
            status={status}
            isAvailable={isAvailable}
            airConditioning={airConditioning}
            transmissionType={transmissionType}
            passengerQuantity={passengerQuantity}
            baggageQuantity={baggageQuantity}
            doorCount={doorCount}
            fuelType={fuelType}
            driveType={driveType}
          />
        </div>
        <div className="car-footer-info">
          <CarVendor
            vendorName={vendorName}
            vendorCode={vendorCode}
          />
          <CarPrice
            rateTotalAmount={rateTotalAmount}
            currencyCode={currencyCode}
            estimatedTotalAmount={estimatedTotalAmount}
          />
        </div>
      </div>
    </div>
  );
}

export default Car;
