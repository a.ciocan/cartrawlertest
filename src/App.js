/* eslint-disable react/prop-types */
import './App.css';
import { Routes, Route } from 'react-router-dom';
import CarList from './pages/CarList/CarList';
import Car from './pages/Car/Car';

function App() {
  return (
    <Routes>
      <Route path="/" element={<CarList />} />
      <Route
        path="/car/:id"
        element={<Car />}
      />
      <Route component={Error} />
    </Routes>
  );
}

export default App;
