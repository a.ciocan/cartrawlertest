import PropTypes from 'prop-types';

function CarPrice({ rateTotalAmount, currencyCode, estimatedTotalAmount }) {
  return (
    <div className="car-price">
      <div className="rate-total-cost">
        Rate Total Cost:
        {' '}
        {rateTotalAmount}
        {' '}
        {currencyCode}
      </div>
      <div className="estimated-total-cost">
        Estimated Total Cost:
        {' '}
        {estimatedTotalAmount}
        {' '}
        {currencyCode}
      </div>
    </div>
  );
}

CarPrice.propTypes = {
  currencyCode: PropTypes.string.isRequired,
  estimatedTotalAmount: PropTypes.string.isRequired,
  rateTotalAmount: PropTypes.string.isRequired,
};

export default CarPrice;
