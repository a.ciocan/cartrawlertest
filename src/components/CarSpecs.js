import PropTypes from 'prop-types';
import Icon from '@mdi/react';
import {
  mdiAirConditioner,
  mdiCarShiftPattern,
  mdiSeatPassenger,
  mdiBagPersonal,
  mdiCarDoor,
} from '@mdi/js';

function CarSpecs({
  vehicleMakeName, vehicleCodeContext, vehicleCode,
  status, isAvailable, airConditioning, transmissionType,
  passengerQuantity, baggageQuantity, doorCount, fuelType, driveType,
}) {
  return (
    <div className="car-specs">
      <div>
        <div className="car-model">{vehicleMakeName}</div>
        <div className="car-code">
          {vehicleCodeContext}
          {' '}
          Code:
          {' '}
          {vehicleCode}
        </div>
        {isAvailable
          ? <div className="car-available">{status}</div>
          : <div className="car-unavailable">{status}</div>}
      </div>
      <div>
        <ul className="feature-list">
          {airConditioning === 'true' && (
          <li className="feature">
            <Icon path={mdiAirConditioner} title="Air Conditioner" size={1} />
            <span>Air Conditioning</span>
          </li>
          )}
          <li className="feature">
            <Icon path={mdiCarShiftPattern} title="Transmission" size={1} />
            <span>{transmissionType}</span>
          </li>
          <li className="feature">
            <Icon path={mdiSeatPassenger} title="Passengers" size={1} />
            <span>
              {passengerQuantity}
              {' '}
              seats
            </span>
          </li>

          <li className="feature">
            <Icon path={mdiBagPersonal} title="Bags" size={1} />
            <span>
              {baggageQuantity}
              {' '}
              bags
            </span>
          </li>

          <li className="feature">
            <Icon path={mdiCarDoor} title="Doors" size={1} />
            <span>
              {doorCount}
              {' '}
              doors
            </span>
          </li>
        </ul>

        <div className="additional-features">
          <div>
            Fuel Type:
            {' '}
            {fuelType}
          </div>
          <div>
            DriveType:
            {' '}
            {driveType}
          </div>
        </div>
      </div>
    </div>
  );
}

CarSpecs.propTypes = {
  airConditioning: PropTypes.string.isRequired,
  baggageQuantity: PropTypes.string.isRequired,
  doorCount: PropTypes.string.isRequired,
  driveType: PropTypes.string.isRequired,
  fuelType: PropTypes.string.isRequired,
  isAvailable: PropTypes.bool.isRequired,
  passengerQuantity: PropTypes.string.isRequired,
  status: PropTypes.string.isRequired,
  transmissionType: PropTypes.string.isRequired,
  vehicleCode: PropTypes.string.isRequired,
  vehicleCodeContext: PropTypes.string.isRequired,
  vehicleMakeName: PropTypes.string.isRequired,
};

export default CarSpecs;
