import PropTypes from 'prop-types';

function CarVendor({ vendorName, vendorCode }) {
  return (
    <div className="car-vendor">
      {vendorName}
      {' '}
      {vendorCode}
    </div>
  );
}

CarVendor.propTypes = {
  vendorCode: PropTypes.string.isRequired,
  vendorName: PropTypes.string.isRequired,
};

export default CarVendor;
