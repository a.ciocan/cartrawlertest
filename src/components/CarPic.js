import PropTypes from 'prop-types';

function CarPic({ pictureURL }) {
  return (
    <div className="pic">
      <div className="preview">
        <img src={pictureURL} alt="" />
      </div>
    </div>
  );
}

CarPic.propTypes = {
  pictureURL: PropTypes.string.isRequired,
};

export default CarPic;
